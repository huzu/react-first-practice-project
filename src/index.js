// Import react and react dom libraries

import React from 'react';
import ReactDOM from 'react-dom';

// Create a react component. There are 2 types of react components, class based and function based. We will create a funtion based componet below. 

function getTime(){
  return (new Date()).toLocaleTimeString()
}


const App = () => {
  const buttonText = 'Click Me !';
  const labelText = 'Enter Name:';  

  return(
    <div>
      <p> Current Time is: {getTime()} </p>
      <label className = "label" htmlFor = "name"> {labelText} </label>
      <input type = "text" id = "name"/>
      <button style={{backgroundColor: 'blue', color: 'white'}}> {buttonText} </button>
    </div>
  );
};

// Take the component and show it on screen.

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);